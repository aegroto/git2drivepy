import glob, os, traceback

from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

def generate_credentials_file():
    gauth = GoogleAuth()
    gauth.LocalWebserverAuth()

    gauth.SaveCredentialsFile("auth/google.json")

def init_drive_client():
    gauth = GoogleAuth()
    gauth.LoadCredentialsFile("auth/google.json")

    drive = GoogleDrive(gauth)
    return drive

drive = init_drive_client()

def create_folder(folder, parent_id):
    parents = [{"id": parent_id}] if parent_id else None

    dpfolder = drive.CreateFile({
        "title": folder,
        "parents": parents,
        "mimeType": "application/vnd.google-apps.folder"
    })

    dpfolder.Upload()

    return dpfolder

def upload_folder(folder, force=False):
    print('Uploading folder "{}"...'.format(folder))

    parent_id = 'root' 

    parent_folders = folder.split("/")

    for i in range(0, len(parent_folders)):
        parent_folder = parent_folders[i]

        dfolder_list = drive.ListFile({
            "q": "'{}' in parents and trashed=false".format(parent_id)
        }).GetList()

        if i == len(parent_folders) - 1:
            # Base folder
            for dfolder in dfolder_list:
                if dfolder["title"] == parent_folder:
                    print("WARNING: Base folder \"{}\" already exists on Drive".format(parent_folder))

                    if not force:
                        print("Soft mode. Aborting...")
                        return

                    else:
                        print("Force mode. Erasing existing folder...")

                        dfolder.Delete()

            dfolder = create_folder(parent_folder, parent_id)
            parent_id = dfolder["id"]
        else:
            # External parent folder
            need_to_create_folder = True

            for dfolder in dfolder_list:
                if dfolder["title"] == parent_folder:
                    print("WARNING: External parent folder \"{}\" already exists on Drive".format(parent_folder))

                    parent_id = dfolder["id"]
                    need_to_create_folder = False
                    break

            if need_to_create_folder:
                dpfolder = create_folder(parent_folder, parent_id)
                parent_id = dpfolder["id"]

    upload_content(folder, parent_id)

def upload_content(path, parent_id):
    files_to_upload = glob.glob('{}/*'.format(path), recursive=True)

    parents = [{"id": parent_id}] if parent_id else None

    for file_path in files_to_upload:
        try:
            if os.path.isfile(file_path):
                print('Uploading file "{}"'.format(file_path))

                dfile = drive.CreateFile({
                    "title" : os.path.basename(file_path),
                    "parents": parents
                })

                dfile.SetContentFile(file_path)
                dfile.Upload()
            elif os.path.isdir(file_path):
                print('Uploading folder "{}"'.format(file_path))

                dfolder = drive.CreateFile({
                    "title": os.path.basename(file_path), 
                    "parents": parents,
                    "mimeType": "application/vnd.google-apps.folder"
                })

                dfolder.Upload()

                upload_content(file_path, dfolder["id"])
        except:
            print("Error while uploading")
            traceback.print_exc()
