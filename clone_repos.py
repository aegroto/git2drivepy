import yaml
import git_utils as git

def clone_repos():
    repositories = yaml.safe_load(open("repositories.yaml"))

    for repository in repositories:
        git.clone(repository["url"], repository["folder"], clear=True)

if __name__ == "__main__":
    clone_repos()