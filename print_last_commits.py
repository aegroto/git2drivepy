import yaml
import git_utils as git

def print_last_commits():
    repositories = yaml.safe_load(open("repositories.yaml"))

    for repository in repositories:
        print("{} {}".format(repository["last_uploaded_commit"])

if __name__ == "__main__":
    print_last_commits()
