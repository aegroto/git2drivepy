import drive_utils

def main():
    drive_utils.generate_credentials_file()

if __name__ == "__main__":
    main()