import yaml
import shell_utils as shell

def clear_repos():
    repositories = yaml.safe_load(open("repositories.yaml"))

    for repository in repositories:
        shell.exec("rm -rf \"{}\"/*".format(repository["folder"]))

if __name__ == "__main__":
    clear_repos()
