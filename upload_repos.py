import yaml
import drive_utils as drive
import git_utils as git

def upload_repos():
    repositories = yaml.safe_load(open("repositories.yaml"))

    for repository in repositories:
        print("Uploading repo \"{}\"".format(repository["folder"]))

        last_commit = git.last_commit(repository["folder"])

        try:
            last_uploaded_commit = repository["last_uploaded_commit"]
        except KeyError:
            last_uploaded_commit = None

        if last_uploaded_commit and last_uploaded_commit == last_commit:
            print("Repository is already updated")
            continue

        drive.upload_folder(repository["folder"], True)

        repository["last_uploaded_commit"] = last_commit
        yaml.dump(repositories, open("repositories.yaml", "w")) 

if __name__ == "__main__":
    upload_repos()
