import shell_utils as shell

def clone(url, folder, clear=False, force=False):
    print('Cloning "{}" in "{}"...'.format(url, folder))

    if clear:
        shell.exec("rm -rf \"{}\"".format(folder))

    shell.exec("mkdir -p \"{}\"".format(folder))

    return shell.exec("git clone {} \"{}\"".format(url, folder))

def pull(folder):
    print('Pulling "{}"'.format(folder))

    return shell.exec("cd \"{}\" && git pull".format(folder))

def lfs_fetch(folder):
    print('Fetching LFS files in "{}"'.format(folder))

    return shell.exec("cd \"{}\" && git lfs install && git lfs fetch".format(folder))

def update(folder):
    print('Updating "{}"...'.format(folder))

    output = pull(folder)
    output = output.stdout.decode('utf-8')

    if "up to date" in output:
        return False
    else:
        return True

def last_commit(folder):
    return shell.exec("cd \"{}\" && git rev-parse HEAD".format(folder)).stdout.decode('utf-8').strip()
