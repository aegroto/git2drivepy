import subprocess 

def exec(cmd):
    # print("Executing \"{}\"...".format(cmd))

    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # print("Output:\n{}".format(result.stdout.decode('utf-8')))

    return result