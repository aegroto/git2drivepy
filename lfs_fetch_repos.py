import yaml
import git_utils as git

def lfs_fetch_repos():
    repositories = yaml.safe_load(open("repositories.yaml"))

    for repository in repositories:
        git.lfs_fetch(repository["folder"])

if __name__ == "__main__":
    lfs_fetch_repos()
