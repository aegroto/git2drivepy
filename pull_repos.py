import yaml
import git_utils as git

def pull_repos():
    repositories = yaml.safe_load(open("repositories.yaml"))

    for repository in repositories:
        git.pull(repository["folder"])

if __name__ == "__main__":
    pull_repos()