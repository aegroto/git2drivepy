import yaml
import git_utils as git
import drive_utils as drive

def update_repos():
    repositories = yaml.safe_load(open("repositories.yaml"))

    for repository in repositories:
        updated = git.update(repository["folder"])
        
        if updated:
            print("Uploading repository...")
            # drive.upload(repository["folder"])
        else:
            print("Repository was up to date")

if __name__ == "__main__":
    update_repos()